from django.db import models

from symposion.proposals.models import ProposalBase


class Proposal(ProposalBase):

    TARGET_USER = 1
    TARGET_BUSINESS = 2
    TARGET_COMMUNITY = 3
    TARGET_DEVELOPER = 4

    TARGET_AUIDENCES = [
        (TARGET_USER, "User"),
        (TARGET_BUSINESS, "Business"),
        (TARGET_COMMUNITY, "Community"),
        (TARGET_DEVELOPER, "Developer"),
    ]

    target_audience = models.IntegerField(choices=TARGET_AUIDENCES)

    recording_release = models.BooleanField(
        default=True,
        help_text="I allow Linux Australia to release any recordings of "
        "presentations covered by this proposal, under the <a "
        "href='https://creativecommons.org/licenses/by-sa/3.0/au/deed.en'> "
        "Creative Commons Attribution-Share Alike Australia 3.0 Licence</a>"
    )

    materials_release = models.BooleanField(
        default=True,
        help_text="I allow Linux Australia to release any other material "
        "(such as slides) from presentations covered by this proposal, under "
        "the <a "
        "href='https://creativecommons.org/licenses/by-sa/3.0/au/deed.en'> "
        "Creative Commons Attribution-Share Alike Australia 3.0 Licence</a>"
    )

    class Meta:
        abstract = True


class TalkProposal(Proposal):

    class Meta:
        verbose_name = "talk proposal"


class TutorialProposal(Proposal):

    class Meta:
        verbose_name = "tutorial proposal"


class MiniconfProposal(Proposal):

    target_audience = models.IntegerField(choices=Proposal.TARGET_AUIDENCES,
                                          default=Proposal.TARGET_DEVELOPER)

    class Meta:
        verbose_name = "miniconf proposal"


class SysAdminProposal(Proposal):

    TYPE_SHORT_PRESENTATION = 1
    TYPE_LIGHTNING_TALK = 2

    TALK_FORMATS = [
        (TYPE_SHORT_PRESENTATION, "Short Presentation (15-25 min)"),
        (TYPE_LIGHTNING_TALK, "Lightning Talk (5-10 min)"),
    ]

    talk_format = models.IntegerField(
        choices=TALK_FORMATS,
        help_text="Please indicate your preferred talk length in the private abstract field below.")

    class Meta:
        verbose_name = "System Administration Miniconf Proposal"


class KernelProposal(Proposal):

    class Meta:
        verbose_name = "Kernel Miniconf Proposal"


class GamesProposal(Proposal):

    TYPE_PRESENTATION = 1
    TYPE_DEMONSTRATION = 2
    TYPE_OTHER = 3

    TALK_FORMATS = [
        (TYPE_PRESENTATION, "Presentation"),
        (TYPE_DEMONSTRATION, "Demonstration"),
        (TYPE_OTHER, "Other"),
    ]

    talk_format = models.IntegerField(choices=TALK_FORMATS)

    class Meta:
        verbose_name = "Games and FOSS Miniconf Proposal"


class OpenHardwareProposal(Proposal):

    TYPE_NORMAL_PRESENTATION = 1
    TYPE_LIGHTNING_TALK = 2

    TALK_FORMATS = [
        (TYPE_NORMAL_PRESENTATION, "Presentation (20 min)"),
        (TYPE_LIGHTNING_TALK, "Lightning Talk (5 min)"),
    ]

    talk_format = models.IntegerField(choices=TALK_FORMATS)

    class Meta:
        verbose_name = "Open Hardware Miniconf Proposal"


class OpenEdProposal(Proposal):

    class Meta:
        verbose_name = "Open Education Miniconf Proposal"


class DevDevProposal(Proposal):

    class Meta:
        verbose_name = "Developers Developers Miniconf Proposal"


class ArtTechProposal(Proposal):

    TYPE_TECH = 1
    TYPE_DEMO = 2
    TYPE_LIGHTNING = 3

    TALK_FORMATS = [
        (TYPE_TECH, "Tech talk (45 min)"),
        (TYPE_DEMO, "Demonstration (20min)"),
        (TYPE_LIGHTNING, "Lightning Talk (10min)"),
    ]

    talk_format = models.IntegerField(
        choices=TALK_FORMATS,
        default=TYPE_TECH,
    )

    EXHIBIT_YES = 1
    EXHIBIT_NO = 2

    EXHIBIT_STATUS = [
        (EXHIBIT_YES, "I am willing and able to exhibit my project on Monday, January 22nd"),
        (EXHIBIT_NO, "I'm unable to exhibit my project")
    ]

    can_exhibit = models.IntegerField(
        choices=EXHIBIT_STATUS,
        default=EXHIBIT_YES,
    )

    exhibition_requirements = models.TextField(
        blank=True,
        default="",
    )

    TARGET_NA = 1

    TARGET_AUDIENCES = [
        (TARGET_NA, "N/A"),
    ]

    target_audience = models.IntegerField(choices=TARGET_AUDIENCES, default=TARGET_NA)

    class Meta:
        verbose_name = "Art+Tech Miniconf Proposal"
