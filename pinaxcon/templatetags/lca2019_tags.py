from django import template
from django.forms import Form


register = template.Library()


@register.filter
def has_required_fields(form):
    for field in form:
        if isinstance(field, Form):
            if has_required_fields(field):
                return True
        if field.field.required:
            return True
    return False


@register.filter
def any_is_void(invoices):
    for invoice in invoices:
        if invoice.is_void:
            return True
    return False