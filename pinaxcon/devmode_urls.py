from django.conf.urls import include, url
from django.contrib.auth.views import login, logout
    
from pinaxcon import urls

urlpatterns = [
    url(r'^accounts/logout', logout, {'template_name': 'admin/logout.html'}),
    url(r'^accounts/login', login, {'template_name': 'admin/login.html'}),
]

urlpatterns += urls.urlpatterns
